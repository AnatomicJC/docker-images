# docker-images

This repository is my playground for docker multiarch images build automation. 

In practice, for any new Dockerfile created, a Gitlab pipeline is automagically generated without the need to edit `.gitlab-ci.yml` file thanks to [Gitlab CI/CD dynamic pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html#dynamic-child-pipelines) feature.

## Stages

1. Docker build: a multiarch image is built with [buildah](https://buildah.io/) and stored in Gitlab registry
2. Docker quality: the image is scanned with [trivy](https://aquasecurity.github.io/) for vulnerabilities
3. Docker Hub push: the image is pushed to Docker Hub

## How it works

The Gitlab CI/CD pipeline is generated with [generate-ci-config](generate-ci-config) file. For debugging purposes, you can execute the file locally to see the generated `.gitlab-ci.yml` used for child pipeline.

```
/bin/sh generate-ci-config
```

Each folder in `Dockerfiles` is a Docker image and can contain multiple Dockerfiles, one per tagged version.

To tag an image with its version, you have to put `ARG VERSION=xxx` in your Dockerfile. It is the only mandatory thing of this repository.

If a file is named `Dockerfile`, the generated docker images will be also tagged as `latest`.

### Example

As an example, let's assume the alpine folder contains 2 Dockerfiles:

The first one is named `Dockerfile`:

```
ARG VERSION=3.15
FROM alpine:${VERSION}

RUN apk upgrade --no-cache
```

It will generate an alpine image with 2 tags:

* latest (as it is named Dockerfile)
* 3.15 (because `ARG VERSION=3.15`)

The second one is named `Dockerfile-3.14`:

```
ARG VERSION=3.14
FROM alpine:${VERSION}

RUN apk upgrade --no-cache
```

This Dockerfile will generate only one tag:

* 3.14

## Multiarch build

The docker images are multiarch ones. If you need your image to be more or less platforms compatible, create a `platform` file with your desired platforms aside your Dockerfile.

As an example, my ansible docker image is amd64 only. Build for all platforms is toooo long.

## Docker images from branches

If you create other branches than the default `main`, the branch name will be added to the tag name.

If the branch is named: `cool-new-feature`, the latest alpine image tag will be named: `latest-cool-new-feature`. This to distinct generated images from main branch and from others.

## About the generated pipelines

A pipeline is triggered each night to rebuild all images to apply operating system security fixes.

If a pipeline is triggered from another branch than main, images won't be automatically built, you have to click on the play button to launch jobs.

If you push on the main branch an edited version of a given Dockerfile:

* the pipeline will trigger only the modified image related job
* if your image depends of another image in this repo, all inter-dependent images jobs will be triggered.

Example:

If you edit the opendistro image, only opendistro jobs will be triggered.

If you edit the alpine image, all jobs who depends of alpine will be triggered (helm, hugo, etc).
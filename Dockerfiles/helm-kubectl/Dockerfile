ARG CI_REGISTRY
ARG VERSION=v3.5.2
FROM ${CI_REGISTRY}/anatomicjc/docker-images/alpine:latest

# Note: Latest version of kubectl may be found at:
# https://github.com/kubernetes/kubernetes/releases
ENV KUBE_LATEST_VERSION="v1.23.4"
# Note: Latest version of helm may be found at:
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION=v3.8.0

ADD "https://dl.k8s.io/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl" /usr/local/bin/

RUN chmod +x /usr/local/bin/kubectl

RUN wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
  && chmod +x /usr/local/bin/helm

RUN apk upgrade --no-cache \
 && apk add --no-cache git coreutils bash curl jq openssh \
 && mkdir -p ~/.ssh \
 && echo ${ANSIBLE_SSH_KEY} | base64 -d > ~/.ssh/id_ed25519 \
 && chmod 400 ~/.ssh/id_ed25519

CMD ["/bin/ash"]

SHELL ["/bin/bash", "-c"]
